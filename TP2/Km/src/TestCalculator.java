import static org.junit.Assert.*;
import org.junit.Test;


public class TestCalculator {

	@Test
	public void testZero() {		
		assertEquals(0.0, new Calculator().calculate(0.0),0);
	}
	@Test
	public void testMinus() {
		try{
			new Calculator().calculate(-2.4);
			fail("Illegal Argument  given");
		}catch(IllegalArgumentException e){
			System.err.println(e.getClass().getName());
		}	
	}
	@Test
	public void testAbove60() {
		assertEquals(40.86, new Calculator().calculate(120.0),0.001);
	}
	
	@Test
	public void testBelow10() {
		assertEquals(13.5, new Calculator().calculate(9.0),0.001);
	}	
	@Test
	public void testBelow40() {
		assertEquals(4, new Calculator().calculate(10.0),0.001);
	}
	
	@Test
	public void testBelow60() {
		assertEquals(22, new Calculator().calculate(40.0),0.001);
	}
	
	@Test
	public void testNull(){
		try{
			new Calculator().calculate(null);
			fail("Null given");
		}catch(NullPointerException e){
			System.err.println(e.getClass().getName());
		}
	}

}
