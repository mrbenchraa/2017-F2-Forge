import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Calculator{
	Double tarif[][] = new Double[][]{
		{1.5,1.0},
		{0.4,1.0},
		{0.55,1.0},
		{6.81,20.0},
	};
	
	Double bornes[][] = new Double[][]{
			{0.0,10.0},
			{10.0,40.0},
			{40.0,60.0},
			{60.0,Double.POSITIVE_INFINITY},
	};
	Double calculate1(Double km){
		if(km == null) throw new NullPointerException();
		if(km < 0 )  throw new IllegalArgumentException();
		
		Double reste = km;
		Double result= 0.0;
		for(int i=0;i<bornes.length;i++){
			if(km >= bornes[i][0] && km < bornes[i][1]){
				for(int j=0;j<i;j++){
					reste-=(bornes[j][1]-bornes[j][0]);
					result+= ( (bornes[j][1]-bornes[j][0]) * tarif[j][0]);
				}
				result+=((int)(reste/tarif[i][1])*tarif[i][0]);
			}
		}
		return result;
	}
	Double calculate(Double km)  throws NullPointerException,IllegalArgumentException{
		
		Double reste = km;
		if(km == null) throw new NullPointerException();
		if(km < 0 )  throw new IllegalArgumentException();
		
		if(km >=0 && km < 10) return (Double) (km*1.5);
		else if(km >= 10 && km < 40){
			
			reste = km - 10;
			return (Double) (10*1.5+reste*0.4);
		}
		else if(km >= 40 && km < 60) {
			
			reste = km - 10 - 30;
			return (Double) ( 10*1.5 + 30*0.4 + reste*0.55);
		}
		else{
			
			reste = km - 10 - 30 - 20;
			return (Double) ( 10*1.5 + 30*0.4 + 20*0.55 + ((int)(reste/20))*6.81);
		}
	}
	
	public static void main(String[] args){
	    Logger logger = LoggerFactory.getLogger("Calculaor");
		if(args.length >= 1){
			try {
				Double number = Double.parseDouble(args[0]);
				Double result = new Calculator().calculate(number);
				Double result2 = new Calculator().calculate1(number);
				if(number == Double.POSITIVE_INFINITY) {
					logger.warn("['" + args[0].subSequence(0, 4) + "...' passed] -  Very long number passed"); 
					return;
				}
				logger.info(number +" km =>  " + result + " euros");
				logger.info(number +" -cakm =>  " + result2 + " euros");
			}catch(NumberFormatException e){
				logger.warn("['" + args[0] + "' passed] -  You can't pass a string");
			}catch(IllegalArgumentException e){
				logger.warn("['" + args[0] + "' passed] -  You can't pass negative km");
			}catch(NullPointerException e){
				logger.warn("['" + args[0] + "' passed] -  You can't pass null km");
			}
		}else{
			logger.error("Please provide a number");
		}
	}
}
